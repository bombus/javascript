const person = {
    name: "Maxim",
    age: 25,
    greet: function() {
        console.log("Greet!");
    }
}

Object.prototype.sayHello = function() {
    console.log("Say hello from prototype");
}

// create object Lena based on prototype 'person'
const lena = Object.create(person);
// lena.age = 34;
// lena.name = "Elena";
// lena.greet = function() {
//     console.log("Greet, Elena");
// }

const str = new String("Bombus");
